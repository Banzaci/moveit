module.exports = {
	use    : [
		'postcss-import',
		'autoprefixer',
		'postcss-css-variables',
		'postcss-custom-selectors',
		'cssnano'
	],
	input  : './resources/stylesheet/main.css',
	output : './public/stylesheet/main.min.css',
	'postcss-import' : {
  		onImport: function(sources) {
  			   global.watchCSS(sources);
  		}
	}
};
