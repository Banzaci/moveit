'use-strict';
const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const KmPrice = require('./components/km-price');
const Cars = require('./components/cars');
const Extra = require('./components/extra');
const DB = require('./components/db');

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    }
});

server
    .connection({
        host:'localhost',
        port:8000
    })
    .register([Inert, Vision], (err) => {

        server.views({
            engines: {
              hbs: require('handlebars')
            },
            relativeTo: __dirname,
            path: 'views'
        });

        server.route({
            method: 'GET',
            path: '/',
            handler: function (request, reply) {
                reply.view('index');
            }
        });

        server.route({
            method: 'GET',
            path: '/order/{id}',
            handler: function (request, reply) {
                const user = DB.get(request.params.id);
                reply({
                    user
                });
            }
        });

        // server.route({
        //     method: 'GET',
        //     path: '/update/{id}',
        //     handler: function (request, reply) {
        //         const user = DB.get(request.params.id);
        //         reply.view('index', { user });
        //     }
        // });

        server.route({
            method: 'GET',
            path: '/receipt/{id}',
            handler: function (request, reply) {
                const orderNumber = Math.round(Math.random(1)*1000);
                const template = `
                    <p>
                        Ett mail har skickats till <strong>${request.params.id}</strong>. Ditt ordernummer är
                        <strong>${orderNumber}</strong>
                    </p>
                    <p>Om du har frågor, kontakta oss på <a href="mailto:flytt@moveit.se">Moveit</a></p>
                `;
                reply({
                    template
                });
            }
        });

        server.route({
            method:'POST',
            path:'/price',
            handler( request, reply ){
                const body = request.payload;
                const distance = body.distance;
                const cars = Cars.calc(body.livingspace, body.extraspace);
                const kmprice = KmPrice.calc(distance);
                const unpacking = Extra.unpacking(body.unpacking);
                const piano = Extra.piano(body.piano);
                const total = (kmprice.total * cars) + (unpacking + piano);

                DB.save(body);

                const template = `
                <p>
                Från ${body.from} till ${body.to}
                </p>
                <ul>
                    <li><strong>Uppskattat pris: ${total}kr. inkl moms.</strong></li>
                    <li>Antal bilar: ${cars}st.</li>
                    <li>Avstånd: ${distance}km á ${kmprice.km}kr (${kmprice.totalKmPrice}kr.)</li>
                    <li>Piano: ${piano ? piano + 'kr.' : ' nej'}</li>
                    <li>Uppackning: ${unpacking ? unpacking + 'kr.' : ' nej'}</li>
                </ul>
                <p>Vi sparar ditt prisförslag i 90 dagar. För att se prisförslaget, klicka på <a href="#" class="view-order" data-user="${body.email}">Länken</a>.</p>`;

                reply({
                    template,
                    userId:body.email
                });
            }
        });
        server.route({
            method: 'GET',
            path: '/javascript/{filename}',
            handler: function (request, reply) {
                reply.file('javascript/'+request.params.filename);
            }
        });
        server.route({
            method: 'GET',
            path: '/stylesheet/{filename}',
            handler: function (request, reply) {
                reply.file('stylesheet/'+request.params.filename);
            }
        });

        server.start(()=>{
            console.log(`Server started at ${server.info.uri}`);
        });
});
