var webpack = require('webpack');
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    context: __dirname,
    entry: "./resources/javascript/app.js",
    devtool: "inline-sourcemap",
    output: {
        path: __dirname + "/public/javascript",
        filename: "main.min.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    },
    plugins:[
        new LiveReloadPlugin({}),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: true })
    ],
};
