const pricePerKm = (distance) => {
    if(distance >= 100){
        return 7;
    } else if(distance >= 50 && distance < 100) {
        return 8;
    } else {
        return 10;
    }
};

const startPriceFunc = (distance) => {
    if(distance >= 100){
        return 10000;
    } else if(distance >= 50 && distance < 100) {
        return 5000;
    } else {
        return 1000;
    }
};

module.exports = {
    calc(distance){

        var start = startPriceFunc(distance);
        var km = pricePerKm(distance);
        var totalKmPrice = (distance * km);
        var total = start + totalKmPrice;

        return {
            start,
            km,
            total,
            totalKmPrice
        };
    }
};
