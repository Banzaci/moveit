module.exports = {
    calc(volume, extra){
        if(extra) {
            volume = parseInt(volume) + parseInt(extra * 2);
        }
        return Math.ceil(volume / 49);
    }
};
