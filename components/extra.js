module.exports = {
    piano(value){
        return value ? 5000 : 0;
    },
    unpacking(value){
        return value ? 1000 : 0;
    }
};
