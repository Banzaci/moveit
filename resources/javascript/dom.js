let doc = document;

export default class Dom {

    constructor( selector, parent ) {
        if(typeof selector === 'object') {
            this.elem = selector;
        } else {
            this.elem = this.element( selector, parent );
        }
    }

    element( selector, parent ){
        if(parent) {
            return parent.querySelector( selector );
        } else if(selector){
            return doc.querySelector( selector );
        } else {
            return this.elem;
        }
    }

    next(){
        return new Dom( this.elem.nextSibling );
    }

    parent(){
        return new Dom( this.elem.parentNode );
    }

    attr(name){
        return this.elem.getAttribute(name);
    }

    createElement( param ){
        let child = new Dom( document.createElement( param ) );
        this.elem.appendChild(child.elem);
        return child;
    }

    elements(selector, parent ){
        if(parent) {
            return parent.querySelectorAll( selector );
        }
        return doc.querySelectorAll( selector );
    }

    children(selector){
        return this.elements(selector);
    }

    find( selector ) {
        return new Dom( selector, this.elem );
    }

    addClass( style ) {
        this.elem.classList.add(style);
        return this;
    }

    html( text ) {
        if(text) {
            this.elem.innerHTML = text;
            return this;
        }
        return this.elem.innerHTML;
    }

    removeClass(className){
        let classNames = this.elem.className;
        if(this.hasClass(className)){
            classNames = classNames.replace(className, '');
            this.elem.className = classNames;
        }
        return this;
    }

    hasClass( className ){
        if ( this.elem.nodeType === 1 && this.elem.className.indexOf( className ) >= 0 ) {
            return true;
        }
        return false;
    }

    value( text ) {
        if(text) {
            this.elem.value = text;
            return this;
        }
        return this.elem.value;
    }

    on(action, callback){
        this.elem.addEventListener(action, callback);
        return this;
    }
}
