import { Req } from './req';
import Dom from './dom';

let storeObject = {};

export const Dispather = {
    events:[],
    listen( evt, callback ){
        this.events[evt] = this.events[evt] || [];
        this.events[evt].push(callback);
    },
    notify(evt, data){
        let notifiers = this.events[evt];
        for(let event in notifiers){
            notifiers[event](data);
        }
    }
};

export function Register( name, module ) {
    new module( new Dom( `[data-module="${name}"]` ) );
}

export function Post( url, success, error, data ) {
    Req('POST', url, success, error, data );
}

export function Get( url, success, error, data={} ) {
    Req('GET', url, success, error, data );
}

export const Store = {
    set(storeName, storeData){
        storeObject[storeName] = storeData;
    },
    get(storeName){
        return storeObject[storeName];
    }
};
