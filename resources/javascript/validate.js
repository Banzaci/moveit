let validations = {
		min( value, req ){
			return value.length <= req;
		},
		max( value, req ){
				return value.length >= req;
		},
		email( value ){
			let emailFormat = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
			return !emailFormat.test( value );
		},
		number(value){
			return !Number.isInteger(value) && value === '';
		}
};

export default function Validator( conditions, value, errors ){
    if(conditions.length === 0){
      return errors;
    }
    var condition = conditions.shift();
    var funcs = condition.split(':');
    var func = funcs[0].trim();
    var req = (funcs.length > 1) ? funcs[1].trim() : '';
    var hasError = validations[func]( value, req );
    errors.push({ func:func, hasError:hasError });
    return Validator.call(this, conditions, value, errors);
}
