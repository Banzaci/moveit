import { Register, Dispather, Post } from '../core';
import Validate from '../validate';
import Dom from '../dom';

Register( 'Order', class {

    constructor(container){

        this.container  = container;
        this.form       =  this.container.find('form');
        this.submit     = this.container.find('#submit');
        this.formInputs = Array.from(this.form.children('input'));
        this.checker    = {};
        this.filteredItems = this.filterReq();

        Dispather.notify('new-header', {
            header:'Välkommen till bohagsflytt.'
        });

        this.formValidateOnBlur();
        this.button();
        this.checkIfReqFieldsAreValid();
        Dispather.listen('user-data', this.populateFields.bind(this));
    }

    populateFields(data){
        for(let key in data.user){
            this.container.find(`#${key}`).value(data.user[key]);
        }
        Dispather.notify('new-header', {
            header:'Prisöversikt.'
        });
        this.container.removeClass('hidden');
    }

    checkIfReqFieldsAreValid(){
        this.filteredItems.forEach((item)=>{
            let errorCheck = Validate(item.reqs.split('|'), item.element.value(), []);
            let noErrors = errorCheck.every((elem)=>{
                return !elem.hasError;
            });
            this.showHideRequire(noErrors, item.element.next());
            this.checker[item.element.attr('id')] = noErrors;
        });
        this.showSubmitButton(this.checkFields(this.checker));
    }

    showHideRequire(noErrors, element){
        if(noErrors) {
            element
                .addClass('glyphicon-ok')
                .removeClass('glyphicon-remove');
        } else {
            element
                .removeClass('glyphicon-ok')
                .addClass('glyphicon-remove');
        }
    }

    filterReq(){

        let elements = [];
        for(let key in this.formInputs){

            let item = this.formInputs[key];
            let reqs  = item.getAttribute('req');

            if(item.nodeType === 1 && reqs){
                let element = new Dom( item );
                elements.push({element, reqs});
            }
        }

        return elements;
    }

    formValidateOnBlur(){
        const items = this.filterReq();
        items.forEach((item)=>{
            item.element.on('blur', (e)=>{
                this.checkIfReqFieldsAreValid();
            });
        });
    }

    checkFields(checker){
        for (let key in checker) {
            if(!checker[key]) return false;
        }
        return true;
    }

    showSubmitButton(show){
        if(show){
            this.submit.removeClass('disabled');
        } else {
            this.submit.addClass('disabled');
        }
    }

    button(){
        this.submit.on('click', this.onClick.bind(this));
    }

    onClick( e ){
        e.preventDefault();
        Post('/price', (response) => {
            this.container.addClass('hidden');
            Dispather.notify('response', response);
        }, (err) => {
            console.log(err);
        }, new FormData (this.form.element()));
    }
});
