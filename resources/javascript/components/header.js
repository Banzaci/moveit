import { Register, Dispather } from '../core';

Register( 'Header', class {

    constructor(container){
        this.container = container;
        this.header = this.container.find('h1');
        Dispather.listen('new-header', this.showHeader.bind(this));
    }

    showHeader(data){
        this.header.html(`${data.header}`);
    }
});
