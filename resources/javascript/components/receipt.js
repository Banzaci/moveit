import { Register, Dispather, Store } from '../core';

Register( 'Receipt', class {

    constructor(container){
        this.container = container;
        this.order = this.container.find('.receipt-from-server');
        this.container.find('.btn-close').on('click', this.close.bind(this));
        Dispather.listen('order', this.showOrder.bind(this));
    }

    close(e){
        window.location.assign('/');
    }

    showOrder(data){
        Dispather.notify('new-header', {
            header:'Tack för din order!'
        });
        this.order.html(data.template);
        this.container.removeClass('hidden');
    }

});
