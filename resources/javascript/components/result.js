import { Register, Dispather, Store, Get } from '../core';

Register( 'Result', class {

    constructor(container){
        this.container = container;
        this.result = this.container.find('.result-from-server');
        this.container.find('.btn-close').on('click', this.close.bind(this));
        Dispather.listen('response', this.showResult.bind(this));
        this.container.find('.btn-order').on('click', this.order.bind(this));
    }

    close(e){
        window.location.assign('/');
    }

    order(e){
        Get(`/receipt/${Store.get('userId')}`, (response) => {
            this.container.addClass('hidden');
            Dispather.notify('order', response);
        }, (err) => {
            console.log(err);
        });

    }

    showResult(data){
        Dispather.notify('new-header', {
            header:'Prisförslag för bohagsflytt.'
        });

        this.result.html(data.template);
        Store.set('userId', data.userId);
        this.container.find('.view-order').on('click', (e)=>{
            Get(`/order/${Store.get('userId')}`, (response) => {
                this.container.addClass('hidden');
                Dispather.notify('user-data', response);
            }, (err) => {
                console.log(err);
            });
        });
        this.container.removeClass('hidden');
    }

});
